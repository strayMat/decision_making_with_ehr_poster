
\documentclass[final,hyperref={pdfpagelabels=false}, xcolor=dvipsnames]{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.0]{beamerposter} % Use the beamerposter package for laying out the poster with a portrait orientation and an a0 paper size

\usetheme{GeoURV} % Use the GeoURV theme supplied with this template

\usepackage[english]{babel} % English language/hyphenation

\usepackage{apacite}
\usepackage{natbib}
\usepackage{amsmath,amsthm,amssymb,latexsym} % For including math equations, theorems, symbols, etc
\usepackage{lipsum}
\usepackage{multirow}
\usepackage{makecell}
\usepackage{amsmath}
\usepackage{amsfonts}  
\usepackage{subcaption}
\usepackage[export]{adjustbox}
%\usepackage{times}\usefonttheme{professionalfonts}  % Uncomment to use Times as the main font
%\usefonttheme[onlymath]{serif} % Uncomment to use a Serif font within math environments

\boldmath % Use bold for everything within the math environment

\usepackage{booktabs} % Top and bottom rules for tables

\graphicspath{{img/}} % Location of the graphics files

% color definition (beamer does not support dvipsnames)
%to find RGB specs of named colors: \convertcolorspec{named}{DarkOrchid}{RGB}\tmpp
 
\definecolor{P}{RGB}{0,224,0}
\definecolor{I}{RGB}{0,112,145}
\definecolor{C}{RGB}{255,148,0}
\definecolor{O}{RGB}{153,51,204}
\definecolor{T}{RGB}{128,128,128}

\newcommand{\eg}{e.g., }

% increase caption sizes
\usepackage{caption}
\captionsetup{font=normalsize}

\title{\huge Causal thinking for decision making on Electronic Health Records: why and how}

\author[shortname]{Matthieu Doutreligne \inst{1} \inst{2}, Tristan Struja,
Judith Abecassis \inst{1}, Claire Morgand \inst{5}, Leo Anthony Celi \inst{3}, Gaël Varoquaux \inst{2}}
\institute[shortinst]{ \inst{1} Haute Autorité de Santé, \inst{2} Inria Soda, \inst{3} Massachussets Institute of Technology, \inst{4} Kantonsspital Aarau, \inst{5} ARS Ile-de-France}

\newcommand{\rightfoot}{m.doutreligne@has-sante.fr}
\newcommand{\leftfoot}{\textbf{Paper:} \url{https://arxiv.org/abs/2308.01605/} | \textbf{Code:} \url{https://github.com/soda-inria/causal_ehr_mimic/}}


\begin{document}

\begin{frame}[t] % The whole poster is enclosed in one beamer frame
    \begin{figure}
        \begin{minipage}{.2\linewidth}
            \caption{\textbf{Step-by-step analytic framework} Deriving
                estimation of treatment effect from observational data confronts
                the analyst with many practical choices, some guided by domain knowledge,
                others by statistical and data insights. Making those choices explicit is
                necessary to ensure robustness and reproducibility.}
        \end{minipage}%
        \hfill%
        \begin{minipage}{0.75\linewidth}
            \includegraphics[width=0.8\linewidth]{complete_inference_flow.pdf}
        \end{minipage}%
    \end{figure}

    \begin{columns}[t] % The whole poster consists of two major columns, each of which can be subdivided further with another \begin{columns} block - the [t] argument aligns each column's content to the top

        \begin{column}{.02\textwidth}\end{column} % Empty spacer column

        \begin{column}{.465\textwidth} % The first column

            %------------
            % OBJECTIVES
            %------------
            \begin{block}{How can data lead to useful clinical decisions?}

                First, individualizing care calls for tailored prediction, \emph{e.g.}
                with machine learning. Second, predictions must be associated with actionable
                interventions to avoid shortcuts or biases in the data. For a given treatment, a
                useful model would distinguish responders from non-responders. This requires
                modeling heterogeneity in causal inference \citep{imbens2015causal}, which faces
                many pitfalls, particularly so on time-varying data, as in electronic health
                records (EHRs) or claims. Here, we  decompose these pitfalls in three steps:
                choice of study design, confounding variables, and estimator. Studying the
                effect of albumin on mortality in sepsis in the Medical Information Mart for
                Intensive Care database (MIMIC-IV), we show that these steps are all important
                to build valid decision making.
            \end{block}

            \begin{block}{Step 1: study design -- Frame the question to avoid biases}



                \begin{table}[h!]
                    \resizebox{\linewidth}{!}{%
                        \begin{tabular}{|l|l|l|l|}
                            \hline
                            \multicolumn{1}{|c|}{\textbf{PICO component}} &
                            \multicolumn{1}{c|}{\textbf{Description}}     &
                            \multicolumn{1}{c|}{\textbf{Notation}}        &
                            \multicolumn{1}{c|}{\textbf{Application on resuscitation
                            fluid}}                                                                                          \\ \hline
                            \textbf{\textcolor{P}{Population}}            & What is the target
                            population of interest?                       & $X \sim \mathbb{P}(X)$,
                            the covariate distribution                    &
                            \makecell[l]{Patients with sepsis in the ICU}                                                    \\ \hline
                            \textbf{\textcolor{I}{Intervention}}          &
                            What is the treatment?                        &
                            \makecell[l]{$A \sim \mathbb{P}(A=1)=p_A$,                                                       \\ the probability to be treated} &
                            Combination of crystalloids and albumin                                                          \\ \hline
                            \textbf{\textcolor{C}{Control}}               & What is the clinically
                            relevant comparator?                          & $1-A \sim 1-p_A$                               &
                            Crystalloids only
                            \\ \hline
                            \textbf{\textcolor{O}{Outcome}}               & \makecell[l]{What are the
                            outcomes to compare?}                         & \makecell[l]{$Y(1), Y(0) \sim \mathbb{P}(Y(1),
                            Y(0))$,                                                                                          \\ the
                            potential outcomes distribution}              & 28-day mortality
                            \\ \hline

                            \textbf{\textcolor{T}{Time}}                  & \makecell[l]{Is the start
                                of follow-up aligned
                            \\with intervention assignment?} & \makecell[l]{N/A}
                                                                          & \makecell[l]{Intervention administered
                            \\ within the first
                            24 hours of admission}                                                                           \\ \hline
                        \end{tabular}%
                    }\\
                    \caption{\textbf{PICO(T)} components help to identify the
                        target trial for the medical question of interest.}\label{table:picot}
                \end{table}


                \begin{table}[p!]
                    \centering\small
                    \resizebox{0.7\columnwidth}{!}{
                        \input{img/caumim/albumin_for_sepsis__obs_1d/table1_24h.tex}%
                    }
                    \\[.5ex]

                    \caption{\textbf{Population characteristics on MIMIC-IV} measured on the first 24
                        hours of ICU stay.}\label{table:albumin_for_sepsis:table1_simple}
                \end{table}

            \end{block}

            %-------------
            % METODOLOGIA
            %-------------
            \begin{block}{Step 2: identification -- List necessary information to answer the causal question}
                Potential predictors --covariates-- should be categorized depending on their
                causal relations with the intervention and the outcome: \emph{confounders} are common causes of the
                intervention and the outcome; \emph{colliders} are caused by both the
                intervention and the outcome; \emph{instrumental variables} are a cause of the
                intervention but not the outcome, \emph{mediators} are caused by the
                intervention and is a cause of the outcome.
                This information derived from medical expertise is best summarized in a causal graph.

                \begin{figure}
                    \begin{minipage}{0.25\linewidth}

                        \caption{\textbf{Causal graph for the Albumin vs crystalloids emulated trial} -- The green arrow indicates the effect studied. Black arrows
                            show causal links known to medical expertise. Dotted red arrows highlight confounders not directly observed. For readability, we
                            draw only the most important edges from an expert point of view. All white nodes corresponds to variables included in our study.}
                    \end{minipage}
                    \hfill
                    \begin{minipage}{0.73\linewidth}

                        \includegraphics[width=\linewidth]{img/dagitty_graph.pdf}
                    \end{minipage}

                \end{figure}

            \end{block}

            \begin{block}{Step 3: estimation -- Choose a model to compute the causal effect of interest}
                \begin{itemize}

                    \item{\textbf{Confounder aggregation:}} Confounders captured via
                    measures collected over multiple time points need to be
                    aggregated at the patient level: \eg first or last value before a time point,
                    or mean aggregation.

                    \item{\textbf{Causal estimators or statistical modeling:}} One can
                    estimate the effect with a model of the outcome (also known
                    as G-formula) and use it as a predictive counterfactual
                    model for all possible treatments for a given patient.
                    Alternatively, one can model the propensity of being treated
                    use it for matching or Inverse Propensity Weighting (IPW).
                    Finally, doubly robust methods model both the outcome and
                    the treatment, benefiting from the convergence of both
                    models: Augmented Inverse Propensity Score (AIPW), Double
                    Robust Machine Learning, or Targeted Maximum Likelihood
                    Estimation (TMLE).

                    \item{\textbf{Estimation models of outcome and treatment:}} The
                    causal estimators use models of the outcome or the treatment
                    --called nuisances-- as they are not the main inference
                    targets in our causal effect estimation problem. Which
                    statistical model is best suited is an additional choice and
                    there is currently no clear best practice. \citep{dorie2019automated}
                    The trade-off lies between simple models risking
                    misspecification of the nuisance parameters versus flexible
                    models risking to overfit the data at small sample sizes.

                \end{itemize}

            \end{block}

        \end{column} % End of the first column

        \begin{column}{.03\textwidth}\end{column} % Empty column

        \begin{column}{.465\textwidth} % The second column
            \begin{block}{Step 4: vibration analysis -- Compare the different sources of bias}

                \begin{figure}[h!]
                    \begin{subfigure}[b]{\linewidth}
                        \caption{Framing -- Immortal Time Bias}\label{fig:vibration:itb}
                        \includegraphics[width=0.85\linewidth, right]{img/caumim/immortal_time_bias_double_robust_forest_agg_first_last__bs_30/immortal_time_bias_double_robust_forest_agg_first_last__bs_30_shared_x_axis.pdf}
                    \end{subfigure}
                    \vfill
                    \begin{subfigure}[b]{\linewidth}
                        \centering
                        \caption{Identification -- confounders choice}\label{fig:vibration:confounders}
                        \includegraphics[width=\linewidth, right]{img/caumim/sensitivity_confounders_albumin_for_sepsis__bs_50/sensitivity_confounders_albumin_for_sepsis__bs_50_main_figure_shared_x_axis.pdf}
                    \end{subfigure}
                    \vfill
                    \begin{subfigure}[b]{\linewidth}
                        \centering
                        \caption{Model selection}\label{fig:vibration:models}
                        \includegraphics[width=0.99\linewidth]{img/caumim/albumin_for_sepsis__obs_1d/estimates_20230712__est_lr_rf__bs_50_shared_x_axis.pdf}
                    \end{subfigure}
                    \vfill
                    \caption{\textbf{Forest plots for the vibration analysis --
                            All three analytical steps are equally important for
                            the validity of the analysis.}The diamonds depict
                        the mean effect and the bar are the 95\% confidence
                        intervals obtained respectively by 30, 30 and 50
                        bootstrap repetitions. For framing and
                        identification, the estimator is a doubly robust
                        learner (AIPW) with random forests for nuisances.
                        Features are aggregated by taking the first and last
                        measurements for all
                        experiments.}\label{fig:vibration_analysis}
                \end{figure}

                \textbf{\ref{fig:vibration:itb}) Framing step:} Poor framing
                introduces time bias: A longer observation period (72h)
                artificially favors the efficacy of Albumin by increasing the
                blank period between inclusion and treatment.

                %\textbf{Doubly robust estimators with non-linear interactions emerge as unbiased estimators.}
                \textbf{\ref{fig:vibration:confounders}) Identification step:}
                Choosing less informed confounders set introduces increasing
                bias in the results.


                \textbf{\ref{fig:vibration:models}) Model selection step:}
                Different estimators give different results. Score matching yields
                unconvincingly high estimates, inconsistent with the published RCT. With
                other causal approaches, using linear estimators for nuisances suggest a
                reduced mortality risk for albumin, while using forests for nuisance models
                points to no effect, which is consistent with the RCT gold standard.

            \end{block}

            \begin{block}{Step 5: treatment heterogeneity -- Compute treatment
                    effects on subpopulations}

                Once the analytical choice for the population effect have been
                calibrated to match previous evidence, we estimate conditional
                treatment effects with a final regression. We highlight
                identifiable subgroups of respondants, opening the path to tailored
                intervention.

                \begin{figure}
                    \begin{minipage}{.25\linewidth}
                        \caption{\textbf{Individual treatment effects} -- The subgroup estimates showed
                            better treatment efficacy for patients older than 60 years, septic shock,
                            and to a lower extent males. The final estimator is ridge regression. The
                            boxes contain the $25^\text{th}$ and $75^\text{th}$ percentiles of the CATE
                            distributions with the median indicated by the vertical line. The whiskers
                            extend to 1.5 times the inter-quartile range of the
                            distribution.}\label{fig:albumin_for_sepsis:cate_results}
                    \end{minipage}%
                    \hfill%
                    \begin{minipage}{0.75\linewidth}
                        \includegraphics[width=\textwidth]{img/caumim/cate_estimates_20230718_w_septic_shock__bs_10/boxplot_est__DML__nuisances__Forests__final_Ridge.pdf}
                    \end{minipage}%
                \end{figure}
            \end{block}

            %------------
            % DISCUSSIÓ
            %------------


            %-------------
            % REFERÈNCIES
            %-------------


            %----------------------------------------------------------------------------------------

        \end{column} % End of the second column

        \begin{column}{.015\textwidth}\end{column} % Empty spacer column

    \end{columns} % End of all the columns in the poster
    \begin{block}{Discussion and conclusion}

        Without causal thinking, machine learning risks not being enough
        for optimal decision making for each and every patient.
        Drawing from machine learning only, to develop personalized medicine can
        replicate non-causal associations such as shortcuts improper for decision
        making. As models can pick up information such as race implicitly from the data, they may propagate biases when building AI models which
        can further reinforce health disparities.

        Adopting causal modeling can create actionable decision-making
        systems that help making the best use of constrained medical
        resources, ultimately reducing inequities.

    \end{block}
    \begin{block}{Bibliography}
        \bibliographystyle{apacite}
        \renewcommand{\bibfont}{\footnotesize{\itshape}}
        %\nocite{*} % Insert publications even if they are not cited in the poster
        \bibliography{references}
    \end{block}

\end{frame} % End of the enclosing frame

\end{document}