# Introduction

Comment éclairer la prise de décision clinique à partir des données de soins de
routine ? Premièrement, des soins individualisés nécessitent des prédictions sur
mesure, par exemple grâce à l'apprentissage automatique. Deuxièmement, les
prédictions doivent être associées à des interventions possibles afin d'éviter
de s'appuyer sur des biais dans les données. Pour un traitement
spécifique, un modèle actionnable permettrait de distinguer les patients
répondants des non-répondants.

# Méthodes

Construire un tel outil nécessite de modéliser l'hétérogénéité par inférence
causale, ce qui pose de nombreux problèmes, en particulier dans le cas de
variables temporelles, comme celles présentes dans les dossiers médicaux
électroniques (EHR) ou les bases médico-administratives. Nous décomposons ces
écueils en trois étapes : le choix de la conception de l'étude, des variables
confondantes et de l'estimateur. Nous comparons ces différentes sources de biais
sur la base de données MIMIC-IV (Medical Information Mart for Intensive Care),
en étudiant l'effet de l'albumine sur la mortalité pour les patients atteints de
sépticémie.

# Résultats

En comparant nos résultats à ceux obtenus par des essais cliniques randomisés
pré-existants, nous montrons que ces étapes sont toutes également importantes
pour construire une aide à la décision non-biaisée. Il est certes possible d'obtenir des
conclusions correctes malgré de petites erreurs, mais ignorer totalement l'une de
ces étapes met en danger la validité de l'analyse.

# Conclusion

Pour que les données des dossiers médicaux électroniques informent la décision
clinique, il faut utiliser un cadre interventionnel. En effet, les algorithmes
d'apprentissage automatique ont souvent extrait des associations non causales
entre l'intervention et le résultat, et ne peuvent donc conduire à des interventions
actionnables.  La méthodologie causale est présente dans certaines études, même
si elle n'est qu'implicite, par le biais d'une bonne compréhension de
l'application. Cependant, un cadre clair permet de s'assurer qu'aucun biais ne
passe à travers les mailles du filet. 
